locals {
  ecs_cluster_name = format("%s-ecs-%s", var.project, terraform.workspace)
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name = local.ecs_cluster_name
}

resource "aws_ecs_cluster_capacity_providers" "example" {
  cluster_name = aws_ecs_cluster.ecs_cluster.name

  capacity_providers = ["FARGATE"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

output "ecs_cluster_id" {
  value = aws_ecs_cluster.ecs_cluster.id
}
