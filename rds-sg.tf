locals {
  sg_rds_name = format("%s-rds-sg-%s", var.project, terraform.workspace)
}

resource "aws_security_group" "sg_rds" {
  name   = local.sg_rds_name
  vpc_id = module.vpc.vpc_id

  ingress {
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
    security_groups = [aws_security_group.lb.id]
    from_port       = 0
    to_port         = 5432
  }

  egress {
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    to_port     = 0
  }
}
