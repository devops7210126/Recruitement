locals {
  ecs_alb_name        = format("%s-ecs-alb-%s", var.project, terraform.workspace)
  ecs_tg_name         = format("%s-ecs-tg-%s", var.project, terraform.workspace)
  https_listener_port = 443
}

module "ecs_alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "9.9.0"

  name               = local.ecs_alb_name
  load_balancer_type = "application"
  internal           = false
  vpc_id             = module.vpc.vpc_id
  subnets            = module.vpc.public_subnets
  security_groups = [
    aws_security_group.lb.id
  ]
}

resource "aws_lb_listener" "http_listener" {
  load_balancer_arn = module.ecs_alb.arn
  port              = local.https_listener_port
  protocol          = "HTTPS"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ecs_tg.arn
  }
}

resource "aws_lb_target_group" "ecs_tg" {
  name        = local.ecs_tg_name
  port        = var.container_port
  vpc_id      = module.vpc.vpc_id
  target_type = "instance"
  protocol    = "HTTP"
}