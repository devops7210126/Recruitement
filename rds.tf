locals {
  rds_name          = format("%s-rds-%s", var.project, terraform.workspace)
  rds_instance_type = "db.t3.medium"
  rds_storage       = 10
}

module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 6.7.0"

  identifier = local.rds_name

  engine            = "postgres"
  engine_version    = "14"
  instance_class    = local.rds_instance_type
  allocated_storage = local.rds_storage
  multi_az          = false # default value, should be switched to 'true' for HA

  db_name  = format("%sRDS%s", var.project, terraform.workspace)
  username = "test-user"
  password = data.aws_secretsmanager_secret_version.password.secret_string
  port     = "5432"

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  iam_database_authentication_enabled = true

  vpc_security_group_ids = [aws_security_group.sg_rds.id]

  subnet_ids = module.vpc.private_subnets

  family = "postgres14"

  major_engine_version = "14"

  deletion_protection = true

  depends_on = [aws_secretsmanager_secret.password]
}

data "aws_secretsmanager_secret" "password" {
  name       = "test-db-password"
  depends_on = [aws_secretsmanager_secret.password]
}

data "aws_secretsmanager_secret_version" "password" {
  secret_id  = data.aws_secretsmanager_secret.password.id
  depends_on = [aws_secretsmanager_secret.password]
}