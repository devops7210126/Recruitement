locals {
  vpc_name                  = format("%s-vpc-%s", var.project, terraform.workspace)
  azs_count                 = length(data.aws_availability_zones.available.zone_ids)
  generated_subnets_public  = formatlist("%s", null_resource.subnets_public.*.triggers.subnet)
  generated_subnets_private = formatlist("%s", null_resource.subnets_private.*.triggers.subnet)
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.8.1"

  name                 = local.vpc_name
  cidr                 = var.aws_vpc_cidr
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = local.generated_subnets_private
  public_subnets       = local.generated_subnets_public
  enable_dns_support   = true
  enable_dns_hostnames = true
}

output "aws_vpc_private_subnets" {
  description = "List of Private subnets"
  value       = module.vpc.private_subnets
}

output "aws_vpc_public_subnets" {
  description = "List of Public subnets"
  value       = module.vpc.public_subnets
}

output "aws_vpc_id" {
  description = "VPC id"
  value       = module.vpc.vpc_id
}

resource "null_resource" "subnets_private" {
  count = local.azs_count
  # 10.10.1.0./24, 10.10.2.0./24, 10.10.3.0./24
  triggers = {
    subnet = cidrsubnet(var.aws_vpc_cidr, 8, count.index + 1)
  }
}


resource "null_resource" "subnets_public" {
  count = local.azs_count
  # 10.10.11.0./24, 10.10.12.0./24, 10.10.13.0./24
  triggers = {
    subnet = cidrsubnet(var.aws_vpc_cidr, 8, count.index + 10 + 1)
  }
}