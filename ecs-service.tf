resource "aws_ecs_task_definition" "ecs_task" {
  family                = var.project
  container_definitions = data.template_file.empty_definition.rendered
}

# Defined to supress errors
data "template_file" "empty_definition" {
  template = file(format("%s/task-definition.json", path.module))
  vars = {
    name               = var.project
    container_port     = var.container_port
    execution_role_arn = aws_iam_role.ecs_task_execution_role.arn
  }
}

resource "aws_ecs_service" "ecs-service" {
  name            = format("%s-%s", var.project, terraform.workspace)
  task_definition = aws_ecs_task_definition.ecs_task.family
  desired_count   = 1
  launch_type     = "FARGATE"
  cluster         = aws_ecs_cluster.ecs_cluster.name

  load_balancer {
    target_group_arn = aws_lb_target_group.ecs_tg.arn
    container_name   = var.project
    container_port   = var.container_port
  }

  lifecycle {
    ignore_changes = [desired_count, task_definition]
  }
}