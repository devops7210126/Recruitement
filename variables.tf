variable "project" {
  default = "recruitment"
  type    = string
}

variable "aws_vpc_cidr" {
  default = "10.10.0.0/16"
  type    = string
}

variable "container_port" {
  default = 8080
  type    = number
}